# L System generator and parser
---
This program expands an L-System for a given number of generations and can draw (with turtle graphics) based on a set 
of rules given by this L-System. it uses Python's `turtle` and `tkinter` librairies.
