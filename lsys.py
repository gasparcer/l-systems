import turtle
import tkinter

axiom = "F"
# line_len = 10
# line_len_scl = 2
# line_width_inc = 2
# angle = 22.5
# angle_inc = 4
state_stack = []


def transform_string(s, depth):
    if depth <= 0:
        return s
    else:
        s = s.replace('F', "FF+[+F-F-F]-[-F+F+F]")
        # s = s.replace("X", "X[-FFF][+FFF]FX")
        # s = s.replace("Y", "YFX[+Y][-Y]")
        return transform_string(s, depth-1)

def draw(s, t, length=10, angle=90, line_len_scl=2, line_width_inc=2, angle_inc=4):
    # Remove unused chars
    s = s.replace('X', '').replace('Y', '')
    t.speed(0)
    line_len = length
    angle = angle

    for char in s:
        if char == 'F':
            t.fd(line_len)
        elif char == '+':
            t.rt(angle)
        elif char == '-':
            t.lt(angle)
        elif char == '[':
            t_pos = t.pos()
            t_heading = t.heading()
            current_state = [t_pos, t_heading]
            state_stack.append(current_state)
        elif char == ']':
            state = state_stack.pop()
            t.pu()
            t.goto(state[0])
            t.setheading(state[1])
            t.pd()
        elif char == '|':
            t.rt(180)
        elif char == '>':
            line_len *= line_len_scl
        elif char == '<':
            line_len /= line_len_scl
        elif char == '(':
            angle -= angle_inc
        elif char == ')':
            angle += angle_inc
        elif char == '#':
            t.width(t.width()+line_width_inc)
        elif char == '!':
            t.width(t.width()-line_width_inc)

turtle = turtle.Turtle()
turtle.lt(90)
turtle.pu()
turtle.fd(-200)
turtle.pd()
draw(transform_string(axiom, 3), turtle, angle=22.5)
turtle.mainloop()

